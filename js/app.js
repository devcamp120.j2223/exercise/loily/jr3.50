const gBASE_URL = "https://devcamp-student.herokuapp.com/users/";
var gIdDelete = "";
// load page
$(document).ready(function () {
    getAllStudent();
});
// hàm nhấn nút xác nhận để xóa học viên
$(".btn-delete").click(function () {
    // call api xóa học viên
    callAPiDeleteStudent();
})
// hàm sửa thông tin học viên
function onBtnEditStudentClick(paramElement) {
    "use strict"
    var vId = $(paramElement).data('id');
    console.log('Chỉnh sửa học viên id: ' + vId);
    window.location.href = "editStudent.html" + "?" + "id=" + vId;

}
// hàm xóa học viên
function onBtnDeleteStudentClick(paramElement) {
    "use strict"
    var vId = $(paramElement).data('id');
    gIdDelete = vId;
    console.log('Xóa học viên id: ' + vId);
    $("#modal-delete").modal("show");
}
// hàm call api lấy tất cả thông tin học viên
function getAllStudent() {
    "use strict"
    $.ajax({
        url: gBASE_URL,
        type: "GET",
        dataType: 'json',
        success: function (data) {
            console.log(data)
            //hàm load thông tin vào table
            loadDataOnTable(data);
            // hàm lắng nghe sự kiện nhấn nút
            addEventListener();
        }
    })
}
// hàm load thông tin tất cả học viên vào table
function loadDataOnTable(paramData) {
    "use strict"
    $('#tbl-student').find('tbody').remove();
    for (let bI of paramData) {
        $('<tr>').append(
            $('<td>').text(bI.name),
            $('<td>').text(bI.birthday),
            $('<td>').text(bI.email),
            $('<td>').text(bI.phone),
            $('<td>').append(
                $(`<a href="#">`)
                    .addClass('text-info edit-student')
                    .html(`<i class="fas fa-edit"></i> Chỉnh sửa`).data('id', bI.id),
                " | ",
                $(`<a href="#">`)
                    .addClass('text-danger delete-student')
                    .html(`<i class="fas fa-edit"></i> Xóa`).data('id', bI.id),
            )
        ).appendTo($('#tbl-student'))
    }
}
// hàm lắng nghe sự kiện nhấn nút
function addEventListener() {
    "use strict"
    $('#tbl-student').on('click', '.edit-student', function () {
        onBtnEditStudentClick(this);
    });

    $('#tbl-student').on('click', '.delete-student', function () {
        onBtnDeleteStudentClick(this);
    });
}
// hàm call api xóa học viên
function callAPiDeleteStudent() {
    $.ajax({
        url: gBASE_URL + gIdDelete,
        type: "DELETE",
        success: function (res) {
            console.log(res)
            alert("xóa thành công!");
            window.location.href = "index.html";
        },
        error: function (res) {
            console.log(res.status)
        }
    })
}

